from django.apps import AppConfig


class TindaApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tinda_api'
